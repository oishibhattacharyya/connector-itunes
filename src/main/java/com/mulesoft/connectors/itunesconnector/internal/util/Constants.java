/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.itunesconnector.internal.util;

public final class Constants {
    //constants used in operations

    /*
    public static final String DOC_TYPE = "docType";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "application/json";
    public static final String UTF_8 = "UTF-8";
    */

    public static final String TERM = "term";
    public static final String ENTITY = "entity";
    public static final String TEST_TERM = "bts";
    public static final String TEST_ENTITY = "musicVideo";

}
