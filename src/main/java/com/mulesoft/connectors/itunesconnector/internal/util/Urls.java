/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.itunesconnector.internal.util;

public final class Urls {
    //Path and Parameters

    public static final String SEARCH = "/search";
    //public static final String address = "https://itunes.apple.com";
}
