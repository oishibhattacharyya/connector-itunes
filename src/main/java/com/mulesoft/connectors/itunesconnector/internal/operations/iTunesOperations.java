/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package com.mulesoft.connectors.itunesconnector.internal.operations;

import com.mulesoft.connectors.itunesconnector.api.*;
import com.mulesoft.connectors.itunesconnector.api.ResponseStatus;
import com.mulesoft.connectors.itunesconnector.internal.config.iTunesConfiguration;
import com.mulesoft.connectors.itunesconnector.internal.connection.iTunesConnection;
//import com.mulesoft.connectors.itunesconnector.internal.error.ErrorProvider;
import com.mulesoft.connectors.itunesconnector.internal.service.iTunesService;
import com.mulesoft.connectors.itunesconnector.internal.service.iTunesServiceImpl;
import org.mule.connectors.commons.template.operation.ConnectorOperations;
import org.mule.runtime.extension.api.annotation.param.*;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.runtime.operation.Result;

import static org.mule.runtime.extension.api.annotation.param.MediaType.*;

public class iTunesOperations extends ConnectorOperations<iTunesConfiguration, iTunesConnection, iTunesService> {

    public iTunesOperations() {
        super(iTunesServiceImpl::new);
    }

    /**
     * Method to get a detailed list of artist's available music videos in the iTunes library.
     *
     * @param configuration Configuration Object
     * @param connection    Connection object.
     * @return status of add request in json format
     */
    @DisplayName(value = "Get Artist Music Videos Details")
    //@Throws(ErrorProvider.class)
    @MediaType(value = ANY, strict = false)
    public Result<GetArtistMusicVideosDetailsDTO, ResponseStatus> getResultOfArtistMusicVideosDetails(@Config iTunesConfiguration configuration, @Connection iTunesConnection connection) {
        return newExecutionBuilder(configuration, connection)
                .execute(iTunesService::getResultOfArtistMusicVideosDetails);
    }
}
