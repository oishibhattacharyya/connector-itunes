/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.itunesconnector.internal.service;

import com.mulesoft.connectors.itunesconnector.api.*;
import com.mulesoft.connectors.itunesconnector.api.ResponseStatus;
import com.mulesoft.connectors.itunesconnector.internal.config.iTunesConfiguration;
import com.mulesoft.connectors.itunesconnector.internal.connection.iTunesConnection;
import com.mulesoft.connectors.itunesconnector.internal.util.iTunesUtil;
import com.mulesoft.connectors.itunesconnector.internal.util.Urls;
import com.mulesoft.connectors.itunesconnector.internal.util.RequestService;
import org.mule.connectors.commons.template.service.DefaultConnectorService;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.http.api.HttpConstants;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;

import java.io.InputStream;

import static com.mulesoft.connectors.itunesconnector.internal.attributes.AttributesUtil.setResponseAttributesForSend;
import static com.mulesoft.connectors.itunesconnector.internal.util.ClassForName.GET_ARTIST_MUSIC_VIDEOS_DETAILS_DTO;
import static com.mulesoft.connectors.itunesconnector.internal.util.Constants.*;
//import static com.mulesoft.connectors.itunesconnector.internal.exception.ExceptionHandler.checkError;
//import static com.mulesoft.connectors.itunesconnector.internal.exception.ExceptionHandler.checkErrorAsync;


public class iTunesServiceImpl extends DefaultConnectorService<iTunesConfiguration, iTunesConnection> implements iTunesService {
    public iTunesServiceImpl(iTunesConfiguration config, iTunesConnection connection) {
        super(config, connection);
    }

    public Result<GetArtistMusicVideosDetailsDTO, ResponseStatus> getResultOfArtistMusicVideosDetails() {
        String strUri = getConfig().getAddress() +  Urls.SEARCH;
        HttpRequest request = getConnection().getHttpRequestBuilder().method(HttpConstants.Method.GET).uri(strUri)
                .addQueryParam(ENTITY, TEST_ENTITY)
                .addQueryParam(TERM, TEST_TERM)
                .build();
        HttpResponse httpResponse = RequestService.requestCall(request, false, getConnection());
        InputStream response = httpResponse.getEntity().getContent();
        //checkError(httpResponse);
        Object dto1 = iTunesUtil.getDtoObject(response, GET_ARTIST_MUSIC_VIDEOS_DETAILS_DTO);
        GetArtistMusicVideosDetailsDTO dto = GetArtistMusicVideosDetailsDTO.class.cast(dto1);
        return Result.<GetArtistMusicVideosDetailsDTO, ResponseStatus>builder().output(dto).attributes(setResponseAttributesForSend(httpResponse)).build();
    }


}