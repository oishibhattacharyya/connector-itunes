/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.itunesconnector.internal.service;

import com.mulesoft.connectors.itunesconnector.api.GetArtistMusicVideosDetailsDTO;
import com.mulesoft.connectors.itunesconnector.api.ResponseStatus;
import org.mule.connectors.commons.template.service.ConnectorService;
import org.mule.runtime.extension.api.runtime.operation.Result;

public interface iTunesService extends ConnectorService {

    Result<GetArtistMusicVideosDetailsDTO, ResponseStatus> getResultOfArtistMusicVideosDetails();
}