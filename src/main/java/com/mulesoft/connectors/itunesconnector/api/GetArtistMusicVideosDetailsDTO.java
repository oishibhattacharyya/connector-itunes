/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.itunesconnector.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({
        "resultCount",
        "results"
})

public class GetArtistMusicVideosDetailsDTO {

    @JsonProperty("resultCount")
    private Integer resultCount;

    @JsonProperty("results")
    private List<Results> results;

    @JsonProperty("resultCount")
    public Integer getResultCount() {
        return resultCount;
    }

    @JsonProperty("resultCount")
    public void setResultCount(Integer resultCount) {
        this.resultCount = resultCount;
    }

    @JsonProperty("results")
    public List<Results> getResults() {
        return results;
    }

    @JsonProperty("results")
    public void setResults(List<Results> results) {
        this.results = results;
    }
}
