/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.itunesconnector.api;

import afu.org.checkerframework.checker.igj.qual.I;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({
        "wrapperType",
        "kind",
        "artistId",
        "collectionId",
        "trackId",
        "artistName",
        "collectionName",
        "trackName",
        "collectionCensoredName",
        "trackCensoredName",
        "artistViewUrl",
        "collectionViewUrl",
        "trackViewUrl",
        "previewUrl",
        "artworkUrl30",
        "artworkUrl60",
        "artworkUrl100",
        "collectionPrice",
        "trackPrice",
        "releaseDate",
        "collectionExplicitness",
        "trackExplicitness",
        "discCount",
        "discNumber",
        "trackCount",
        "trackNumber",
        "trackTimeMillis",
        "country",
        "currency",
        "primaryGenreName"
})

public class Results {

    @JsonProperty("wrapperType")
    private String wrapperType;

    @JsonProperty("kind")
    private String kind;

    @JsonProperty("artistId")
    private Integer artistId;

    @JsonProperty("collectionId")
    private Integer collectionId;

    @JsonProperty("trackId")
    private Integer trackId;

    @JsonProperty("artistName")
    private String artistName;

    @JsonProperty("collectionName")
    private String collectionName;

    @JsonProperty("trackName")
    private String trackName;

    @JsonProperty("collectionCensoredName")
    private String collectionCensoredName;

    @JsonProperty ("trackCensoredName")
    private String trackCensoredName;

    @JsonProperty ("artistViewUrl")
    private String artistViewUrl;

    @JsonProperty ("collectionViewUrl")
    private String collectionViewUrl;

    @JsonProperty ("trackViewUrl")
    private String trackViewUrl;

    @JsonProperty ("previewUrl")
    private String previewUrl;

    @JsonProperty ("artworkUrl30")
    private String artworkUrl30;

    @JsonProperty ("artworkUrl60")
    private String artworkUrl60;

    @JsonProperty ("artworkUrl100")
    private String artworkUrl100;

    @JsonProperty ("collectionPrice")
    private Number collectionPrice;

    @JsonProperty ("trackPrice")
    private Number trackPrice;

    @JsonProperty ("releaseDate")
    private String releaseDate;

    @JsonProperty ("collectionExplicitness")
    private String collectionExplicitness;

    @JsonProperty ("trackExplicitness")
    private String trackExplicitness;

    @JsonProperty ("discCount")
    private Integer discCount;

    @JsonProperty ("discNumber")
    private Integer discNumber;

    @JsonProperty ("trackCount")
    private Integer trackCount;

    @JsonProperty ("trackNumber")
    private Integer trackNumber;

    @JsonProperty ("trackTimeMillis")
    private Integer trackTimeMillis;

    @JsonProperty ("country")
    private String country;

    @JsonProperty ("currency")
    private String currency;

    @JsonProperty ("primaryGenreName")
    private String primaryGenreName;

    ///////////////////////////////////////////////////

    @JsonProperty ("wrapperType")
    public String getWrapperType () { return wrapperType; }

    @JsonProperty ("wrapperType")
    public void setWrapperType ( String wrapperType) { this.wrapperType = wrapperType; }

    @JsonProperty("kind")
    public String getKind () { return kind; }

    @JsonProperty("kind")
    public void setKind (String kind) { this.kind = kind; }

    @JsonProperty("artistId")
    public Integer getArtistId () { return artistId; }

    @JsonProperty("artistId")
    public void setArtistId (Integer artistId) { this.artistId = artistId; }

    @JsonProperty("collectionId")
    public Integer getCollectionId () { return collectionId; }

    @JsonProperty("collectionId")
    public void setCollectionId (Integer collectionId) { this.collectionId = collectionId; }

    @JsonProperty("trackId")
    private Integer getTrackId () {return trackId; }

    @JsonProperty("trackId")
    public void setTrackId (Integer trackId) {this.trackId = trackId; }

    @JsonProperty("artistName")
    private String getArtistName () { return artistName; }

    @JsonProperty("artistName")
    public void setArtistName (String artistName) { this.artistName = artistName; }

    @JsonProperty("collectionName")
    private String getCollectionName () {return collectionName; }

    @JsonProperty("collectionName")
    public void setCollectionName (String collectionName) {this.collectionName = collectionName; }

    @JsonProperty("trackName")
    private String getTrackName () {return trackName;}

    @JsonProperty("trackName")
    public void setTrackName (String trackName) {this.trackName = trackName;}

    @JsonProperty("collectionCensoredName")
    private String getCollectionCensoredName () {return collectionCensoredName;}

    @JsonProperty("collectionCensoredName")
    public void setCollectionCensoredName (String collectionCensoredName) {this.collectionCensoredName = collectionCensoredName;}

    @JsonProperty ("trackCensoredName")
    private String getTrackCensoredName () {return trackCensoredName;}

    @JsonProperty ("trackCensoredName")
    public void setTrackCensoredName (String trackCensoredName) {this.trackCensoredName = trackCensoredName;}

    @JsonProperty ("artistViewUrl")
    private String getArtistViewUrl () {return artistViewUrl;}

    @JsonProperty ("artistViewUrl")
    public void setArtistViewUrl (String artistViewUrl) {this.artistViewUrl = artistViewUrl;}

    @JsonProperty ("collectionViewUrl")
    private String getCollectionViewUrl () {return collectionViewUrl;}

    @JsonProperty ("collectionViewUrl")
    public void setCollectionViewUrl (String collectionViewUrl) {this.collectionViewUrl = collectionViewUrl;}

    @JsonProperty ("trackViewUrl")
    private String getTrackViewUrl () {return trackViewUrl;}

    @JsonProperty ("trackViewUrl")
    public void setTrackViewUrl (String trackViewUrl) {this.trackViewUrl = trackViewUrl;}

    @JsonProperty ("previewUrl")
    private String getPreviewUrl () {return previewUrl;}

    @JsonProperty ("previewUrl")
    public void setPreviewUrl (String previewUrl) {this.previewUrl = previewUrl;}

    @JsonProperty ("artworkUrl30")
    private String getArtworkUrl30 () {return artworkUrl30;}

    @JsonProperty ("artworkUrl30")
    public void setArtworkUrl30 (String artworkUrl30) {this.artworkUrl30 = artworkUrl30;}

    @JsonProperty ("artworkUrl60")
    private String getArtworkUrl60 () {return artworkUrl60;}

    @JsonProperty ("artworkUrl60")
    public void setArtworkUrl60 (String artworkUrl60) {this.artworkUrl60 = artworkUrl60;}

    @JsonProperty ("artworkUrl100")
    private String getArtworkUrl100 () {return artworkUrl100;}

    @JsonProperty ("artworkUrl100")
    public void setArtworkUrl100 (String artworkUrl100) {this.artworkUrl100 = artworkUrl100;}

    @JsonProperty ("collectionPrice")
    private Number getCollectionPrice () {return collectionPrice;}

    @JsonProperty ("collectionPrice")
    public void setCollectionPrice (Number collectionPrice) {this.collectionPrice = collectionPrice;}

    @JsonProperty ("trackPrice")
    private Number getTrackPrice () {return trackPrice;}

    @JsonProperty ("trackPrice")
    public void setTrackPrice (Number trackPrice) {this.trackPrice = trackPrice;}

    @JsonProperty ("releaseDate")
    private String getReleaseDate () {return releaseDate;}

    @JsonProperty ("releaseDate")
    public void setReleaseDate (String releaseDate) {this.releaseDate = releaseDate;}

    @JsonProperty ("collectionExplicitness")
    private String getCollectionExplicitness () {return collectionExplicitness;}

    @JsonProperty ("collectionExplicitness")
    public void setCollectionExplicitness (String collectionExplicitness) {this.collectionExplicitness = collectionExplicitness;}

    @JsonProperty ("trackExplicitness")
    private String getTrackExplicitness () {return trackExplicitness;}

    @JsonProperty ("trackExplicitness")
    public void setTrackExplicitness (String trackExplicitness) {this.trackExplicitness = trackExplicitness;}

    @JsonProperty ("discCount")
    private Integer getDiscCount () {return discCount;}

    @JsonProperty ("discCount")
    public void setDiscCount (Integer discCount) {this.discCount = discCount;}

    @JsonProperty ("discNumber")
    private Integer getDiscNumber () {return discNumber;}

    @JsonProperty ("discNumber")
    public void setDiscNumber (Integer getDiscNumber) {this.discNumber = discNumber;}

    @JsonProperty ("trackCount")
    private Integer getTrackCount () {return trackCount;}

    @JsonProperty ("trackCount")
    public void setTrackCount (Integer trackCount) {this.trackCount = trackCount;}

    @JsonProperty ("trackNumber")
    private Integer getTrackNumber () {return trackNumber;}

    @JsonProperty ("trackNumber")
    public void setTrackNumber (Integer trackNumber) {this.trackNumber = trackNumber;}

    @JsonProperty ("trackTimeMillis")
    private Integer getTrackTimeMillis () {return trackTimeMillis;}

    @JsonProperty ("trackTimeMillis")
    public void setTrackTimeMillis (Integer trackTimeMillis) {this.trackTimeMillis = trackTimeMillis;}

    @JsonProperty ("country")
    private String getCountry () {return  country;}

    @JsonProperty ("country")
    public void setCountry (String  country) {this.country =  country;}

    @JsonProperty ("currency")
    private String getCurrency () {return currency;}

    @JsonProperty ("currency")
    public void setCurrency (String currency) {this.currency = currency;}

    @JsonProperty("primaryGenreName")
    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    @JsonProperty("primaryGenreName")
    public void setPrimaryGenreName(String primaryGenreName) {
        this.primaryGenreName = primaryGenreName;
    }

}